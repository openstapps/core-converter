# [0.1.0](https://gitlab.com/openstapps/core-converter/compare/v0.0.1...v0.1.0) (2018-11-29)


### Features

* parameterize cli ([0ef3124](https://gitlab.com/openstapps/core-converter/commit/0ef3124))



## [0.0.1](https://gitlab.com/openstapps/core-converter/compare/2b1ab00...v0.0.1) (2018-11-29)


### Features

* add core converter ([2b1ab00](https://gitlab.com/openstapps/core-converter/commit/2b1ab00))



