/*
 * Copyright (C) 2018 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Logger} from '@openstapps/logger';
import {each} from 'async';
import * as fs from 'fs';
import {existsSync, mkdirSync, readFileSync} from 'fs';
import * as glob from 'glob';
import {Schema as JSONSchema} from 'jsonschema';
import {basename, join, resolve} from 'path';
import {DEFAULT_CONFIG, Definition, SchemaGenerator} from 'ts-json-schema-generator';
import {createFormatter} from 'ts-json-schema-generator/dist/factory/formatter';
import {createParser} from 'ts-json-schema-generator/dist/factory/parser';
import {createProgram} from 'ts-json-schema-generator/dist/factory/program';
import * as ts from 'typescript';

const logger = new Logger();

interface SchemaWithDefinitions extends JSONSchema {
  definitions: { [name: string]: Definition };
}

function isSchemaWithDefinitions(schema: JSONSchema): schema is SchemaWithDefinitions {
  return typeof schema.definitions !== 'undefined';
}

function isString(obj: string | undefined): obj is string {
  return typeof obj === 'string';
}

function isStringArray(obj: string[] | undefined): obj is string[] {
  return Array.isArray(obj) && obj.length > 0 && obj.every((str) => typeof str === 'string');
}

/**
 * StAppsCore converter
 *
 * Converts TypeScript source files to JSON schema files
 */
export class SCConverter {
  private generator: SchemaGenerator;

  /**
   * Create a new converter
   *
   * @param path Path to the project
   */
  constructor(path: string) {
    // set config for schema generator
    const config = {
      ...DEFAULT_CONFIG,
      // expose: 'exported' as any,
      // jsDoc: 'extended' as any,
      path: join(path, '**/*.ts'),
      sortProps: true,
      topRef: false,
      type: 'SC',
    };

    // create TypeScript program from config
    const program: ts.Program = createProgram(config);

    // create generator
    this.generator = new SchemaGenerator(
      program,
      createParser(program, config),
      createFormatter(config),
    );
  }

  /**
   * Get schema for specific StAppsCore type
   *
   * @param {string} type Type to get the schema for
   * @param {string} version Version to set for the schema
   * @returns {Schema} Generated schema
   */
  getSchema(type: string, version: string): JSONSchema {
    // generate schema for this file/type
    const schema: JSONSchema = this.generator.createSchema('SC' + type);

    // set id of schema
    schema.id = 'https://core.stapps.tu-berlin.de/v' + version + '/lib/schema/' + type + '.json';

    if (isSchemaWithDefinitions(schema)) {
      // add self reference to definitions
      schema.definitions['SC' + type] = Object.assign({}, schema.properties);
    }

    return schema;
  }
}

/**
 * Convert TypeScript things to JSON schema files
 *
 * @param path Path to the project (where used *.ts files are, e.g. 'src/core')
 * @param inputDirs Directories (relative paths) to get source files from
 * @param outputDir Directory (relative path) to save output files to
 * @returns Promise that resolves with the amount of converted files
 */
export function convertFiles(path: string, inputDirs: string[], outputDir: string): Promise<number> {
  // check if correct source directories paths are provided
  if (!isStringArray(inputDirs)) {
    return Promise.reject('Invalid source directories provided');
  }

  // check if correct output directory path is provided
  if (!isString(outputDir)) {
    return Promise.reject('Invalid output directory provided');
  }

  return new Promise((promiseResolve, promiseReject) => {
    // read package json
    const pkgJson = JSON.parse(readFileSync('package.json').toString());

    // create schema directory if it does not exist
    if (!existsSync(outputDir)) {
      mkdirSync(outputDir);
    }

    logger.info('Create a new converter...');

    // initialize a new converter
    const converter = new SCConverter(path);

    logger.log('Created a new converter.');
    let totalFiles = 0;
    // convert things and query (protocol) types (validation of the input from the client)
    each(inputDirs, (inputDir, callback) => {
      // find all TypeScript files in path
      glob(resolve(inputDir, '**/*.ts'), {}, (err, files) => {
        if (err) {
          promiseReject(err);
          return;
        }

        logger.info('Found ' + files.length + ' TypeScript source files to convert.');

        // iterate over files
        files.forEach((file) => {
          // get StAppsCore type from filename
          const type = basename(file, '.ts');

          // stringify/pretty print the schema
          const stringifiedSchema = JSON.stringify(converter.getSchema(type, pkgJson.version), null, 2);

          // write schema to file
          fs.writeFileSync(join(outputDir, type + '.json'), stringifiedSchema);

          logger.log('Converted ' + file + ' and saved as `' + join(outputDir, type + '.json') + '`.');
          totalFiles = totalFiles + files.length;
        });
        callback();
      });
    }, (err) => {
      if (err) {
        promiseReject(err);
        return;
      } else {
        promiseResolve(totalFiles);
      }
    });
  });
}
