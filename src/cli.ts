/*
 * Copyright (C) 2018 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Logger} from '@openstapps/logger';
import * as commander from 'commander';
import {readFileSync} from 'fs';
import {join} from 'path';
import {convertFiles} from './converter';

const pkgJson = JSON.parse(readFileSync(join(__dirname, '..', 'package.json')).toString());
let path: string = '';
let outDir: string = '';
let inDirs: string[] = [];

const logger = new Logger();

commander
  .version(pkgJson.version);

commander
  .description('create JSON schema files from TypeScript models')
  .arguments('<path> <outdir> <indirs...>')
  .action((p, o, i) => {
    path = p;
    outDir = o;
    inDirs = i;
    convertFiles(path, inDirs, outDir).then(() => {
      logger.ok('Finished conversion.');
    }, (err: Error) => {
      logger.error(err);
    });
  });

commander.parse(process.argv);

[path, outDir, inDirs].forEach((arg) => {
  if (arg.length === 0) {
    logger.error('Arguments are required.');
    process.exit(1);
  }
});
