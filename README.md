# README

StAppsCore Converter - converter of TypeScript models into JSON schema files

## What is StAppsCore?

The StAppsCore (SC) is a generalized model of data.

SC is based on the idea of [schema.org](https://www.schema.org). The SC structures data by describing objects as so called SC-types like: Event, Place, Date, Person, ...

Data sources of the same type of data are assigned to the same SC-type.

## What is StAppsCore Converter?

The StAppsCore Converter is a tool for converting SC-types (TypeScript) into JSON schema files.

JSON schema files are needed for run-time validation of SC-type objects, as this is a tedious task to do using SC-types defined in TypeScript (not possible without additional coding). That said, StAppsCore Converter practically prepares SC-types to be used for object validation (determining wether an JavaScript/JSON object is a valid object of the corresponding SC-type) using StAppsCore Validator.

## How to use StAppsCore Converter?

Installation of the npm package (using `npm install`) makes the tool available as an executable with the name `stapps-convert`.

The command `stapps-convert` can then be called using 3 required arguments:

```bash
node_modules/.bin/stapps-convert <path> <outdir> <indirs...>
```
where:
- `<path>` is path to the project (where used *.ts files are, e.g. 'src/core',
- `<outdir>` is directory (relative path) to save output files to, e.g. `lib/schema`,
- `<indirs...>` are directories (relative paths) to get source files from, e.g. `src/core/things` `src/core/protocol/`.

Complete command with the example arguments is then:
```bash
node_modules/.bin/stapps-convert src/core lib/schema src/core/things src/core/protocol/
```

Inside of a script in `package.json` or if the npm package is installed globally, the tool `stapps-convert` can be called without its local path (`node_modules/.bin`):

```bash
stapps-convert <path> <outdir> <indirs...>
```
